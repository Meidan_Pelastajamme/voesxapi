# VOESXAPI

A golang package to easily interact with the [voe's](https://voe.sx/api-1-reference-index) api.
For general api doc I recommend [their documentation](https://voe.sx/api-1-reference-index) as it's pretty well crafted.

### Getting started
```go
package main

import(
    "fmt"
	
    voe "codeberg.org/Meidan_Pelastajamme/voesxapi"
)

func main() {
    voe.Rate_limit()

    voe_res_next_url, err := voe.Get_next_server(VOE_API_KEY)
    if err != nil {
    	fmt.Println(err)
	return 
    }
    upload_url := voe_res_next_url.Result
    fmt.Println("[ URL ] " + upload_url)

    file_path := "videos/video.mp4"

    voe.Rate_limit()
    voe_res_upload, err := voe.Post_upload(VOE_API_KEY, upload_url, file_path)
    if err != nil {
        fmt.Println("Error uploading file:", err)
        return 
    }
    fmt.Println("[ OUTCOME ] " + voe_res_upload.Message)
}

```

For more info _go_ read the source code of the lib, it is pretty simple and self explanatory.

NOTE: Yes I now my functions and variables are snake case, so what ? - Do not use this library if you feel intimidated by primal notation (big L to go lang's way of exporting function)

### TODO

##### General directions and features
- [ ] Abstract the get requests to a function (func (url string, params UrlParams) (string, error)
- [ ] Segregate the api V1 to make place for a future API V2 (as [their docs](https://voe.sx/api-1-reference-index) say that it's API 1 (maybe ask support)
- [ ] 

##### API V1 Implementation Completion
- [ ] GET  - `/api/account/info`
- [ ] GET  - `/api/account/stats`
- [x] GET  - `/api/upload/server`
- [x] POST - `delivery-node-***.voe-network.net/***/01`
- [ ] GET  - `/api/upload/url`
- [ ] GET  - `/api/file/clone`
- [ ] GET  - `/api/file/info`
- [ ] GET  - `/api/file/list`
- [ ] GET  - `/api/file/rename`
- [ ] GET  - `/api/file/set_folder`
- [ ] GET  - `/api/file/delete`
- [ ] GET  - `/api/folder/list`
- [ ] GET  - `/api/folder/create`
- [ ] GET  - `/api/folder/rename`
- [ ] GET  - `/api/files/deleted`
- [ ] GET  - `/api/dmca/list`
- [ ] GET  - `/api/settings/domain`
- [ ] GET  - `/api/reseller/premium/generate`

