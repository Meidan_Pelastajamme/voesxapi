package voesxapi

type VoeGetNextServ struct {
	ServerTime string `json:"server_time"`
	Msg        string `json:"msg"`
	Message    string `json:"message"`
	Status     int    `json:"status"`
	Success    bool   `json:"success"`
	Result     string `json:"result"`
}

type VoePostUpload struct {
	Success bool   `json:"success"`
	Message string `json:"message"`
	File    struct {
		ID                int    `json:"id"`
		FileCode          string `json:"file_code"`
		FileTitle         string `json:"file_title"`
		EncodingNecessary bool   `json:"encoding_necessary"`
	} `json:"file"`
}
