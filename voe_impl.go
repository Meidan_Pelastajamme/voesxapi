package voesxapi

import(
    "fmt"
	"log"
	"io"

	"net/http"
	
    "bytes"
    "io/ioutil"
    "mime/multipart"
	"encoding/json"
)

func Get_next_server(api_key string) (VoeGetNextServ, error) {
	url := "https://voe.sx/api/upload/server"

	res, err := http.Get(url + "?key=" + api_key)
	if err != nil { return VoeGetNextServ{}, err }

	body, err := io.ReadAll(res.Body)
	if err != nil { return VoeGetNextServ{}, err }

	res.Body.Close()
	if res.StatusCode > 299 {
		log.Fatalf("Response failed with status code: %d and\nbody: %s\n", res.StatusCode, body)
	}

	var unmarshaled_res VoeGetNextServ
	err = json.Unmarshal(body, &unmarshaled_res)
	if err != nil { return VoeGetNextServ{}, err }
	

	return unmarshaled_res, nil
}

func Post_upload(api_key string, upload_url string, file_path string) (VoePostUpload, error) {
    file_data, err := ioutil.ReadFile(file_path)
    if err != nil {
        return VoePostUpload{}, err
    }

    // Create a multipart form
    body := new(bytes.Buffer)
    form := multipart.NewWriter(body)

    // Add fields: key & file 
    if err := form.WriteField("key", api_key); err != nil {
        return VoePostUpload{}, err
    }
    part, err := form.CreateFormFile("file", file_path) // TODO: Make sure the path includes only the file name
    if err != nil {
        return VoePostUpload{}, err
    }
    if _, err := part.Write(file_data); err != nil {
        return VoePostUpload{}, err
    }

    if err := form.Close(); err != nil {
        return VoePostUpload{}, err
    }

    // Create the POST request
    req, err := http.NewRequest("POST", upload_url, body)
    if err != nil {
        return VoePostUpload{}, err
    }
    req.Header.Set("Content-Type", form.FormDataContentType())

    // Send the request
    client := &http.Client{}
    resp, err := client.Do(req)
    if err != nil {
        return VoePostUpload{}, err
    }
    if resp.StatusCode != http.StatusOK {
        return VoePostUpload{}, fmt.Errorf("Upload failed with status: %d", resp.StatusCode)
    }

	// Read JSON response body
    defer resp.Body.Close()
    bodyBytes, err := ioutil.ReadAll(resp.Body)
    if err != nil {
        return VoePostUpload{}, err
    }

    var unmarshaled_res VoePostUpload
    err = json.Unmarshal(bodyBytes, &unmarshaled_res)
    if err != nil {
        return VoePostUpload{}, err
    }

    return unmarshaled_res, nil
}
