package voesxapi

import(
	"time"
)

// Rate limit: 9 requests / 3 seconds
const REQUEST_DELAY = 1 // true value = 1/3 req per sec

func Rate_limit() {
	time.Sleep(REQUEST_DELAY * time.Second)
}
